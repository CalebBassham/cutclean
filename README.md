# Cut Clean

Ores and food are dropped in their smelted variety; for example, iron ore is dropped as an iron ingot and raw chicken
is dropped as cooked chicken.

### Installation

1. Install the scenario manager; instructions can be found [here][scenario manager]. 
2. Download this plugin from [here][plugin download].
3. Place the downloaded plugin in the `plugins` directory of your server.

### Scenario Manager

The scenario manager is required in order of this plugin to work. You can learn more about what the scenario manger does,
how to use it, what languages it supports, how to install it, and how to integrate it with 
your server for a seamless experience [here][scenario manager].

[scenario manager]: https://gitlab.com/CalebBassham/ScenarioManager
[plugin download]: https://gitlab.com/CalebBassham/cutclean/-/jobs/artifacts/master/raw/build/libs/CutClean.jar?job=package
