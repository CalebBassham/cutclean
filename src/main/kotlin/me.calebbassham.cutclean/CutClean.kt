package me.calebbassham.cutclean

import me.calebbassham.scenariomanager.api.SimpleScenario
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.entity.ExperienceOrb
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.ItemSpawnEvent

class CutClean : SimpleScenario(), Listener {

    @EventHandler
    fun itemSpawn(e: ItemSpawnEvent) {
        if (!scenarioManager.gameWorldProvider.isGameWorld(e.entity.world)) return

        val smelted = when(e.entity.itemStack.type) {
            Material.IRON_ORE -> Material.IRON_INGOT
            Material.GOLD_ORE -> Material.GOLD_INGOT
            Material.POTATO_ITEM -> Material.BAKED_POTATO
            Material.RAW_BEEF -> Material.COOKED_BEEF
            Material.RAW_CHICKEN -> Material.COOKED_CHICKEN
            Material.RAW_FISH -> Material.COOKED_FISH
            Material.PORK -> Material.GRILLED_PORK
            Material.MUTTON -> Material.COOKED_MUTTON
            Material.RABBIT -> Material.COOKED_RABBIT
            else -> return
        }

        if (e.entity.itemStack.type == Material.GOLD_ORE || e.entity.itemStack.type == Material.IRON_ORE) {
            e.location.world.spawnEntity(e.location, EntityType.EXPERIENCE_ORB).apply {
                val orb  = this as? ExperienceOrb ?: return
                orb.experience = e.entity.itemStack.amount
            }
        }

        e.entity.itemStack.type = smelted

    }

}