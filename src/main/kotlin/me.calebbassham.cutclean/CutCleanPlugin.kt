package me.calebbassham.cutclean

import me.calebbassham.scenariomanager.api.scenarioManager
import org.bukkit.plugin.java.JavaPlugin

class CutCleanPlugin : JavaPlugin() {

    override fun onEnable() {
        scenarioManager.register(CutClean(), this)
    }

}